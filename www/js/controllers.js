angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $timeout) {
  // Form data for the login modal
  $scope.loginData = {};

  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/login.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.loginModal = modal;
  });
  
  // Triggered in the login modal to close it
  $scope.closeLogin = function() {
    $scope.loginModal.hide();
  };

  // Open the login modal
  $scope.login = function() {
    $scope.loginModal.show();
  };

  // Perform the login action when the user submits the login form
  $scope.doLogin = function() {
    console.log('Doing login', $scope.loginData);

    // Simulate a login delay. Remove this and replace with your login
    // code if using a login system
    $timeout(function() {
      $scope.closeLogin();
    }, 1000);
  };
})


.controller('PlaylistsCtrl', function($scope) {
  $scope.playlists = [];
})

.controller('PlaylistCtrl', function($scope, $stateParams) {
})

.controller('staffCtrl', function ($scope, $http){
        //$http.get("http://www.w3schools.com/angular/customers_mysql.php")
        $http.get("http://comech.dev/ionic/functions.php?action=staff")
            .success(function(response){
                $scope.names = response.records;
            });
    })

.controller('DailyStatsCtrl', function ($scope, $http){
        //$http.get("http://www.w3schools.com/angular/customers_mysql.php")
        $http.get("http://comech.dev/ionic/functions.php?action=daily")
            .success(function(getDaily){
                $scope.daily = getDaily.jobsInCal;
                // $scope.InCal = getDaily.amountOfJobs;
            });
    })
