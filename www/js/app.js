// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers'])

    .run(function ($ionicPlatform) {
        $ionicPlatform.ready(function () {
            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)
            if (window.cordova && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
            }
            if (window.StatusBar) {
                // org.apache.cordova.statusbar required
                StatusBar.styleDefault();
            }
        });
    })

    .config(function ($stateProvider, $urlRouterProvider) {
        $stateProvider

        .state('app', {
            url: "/app",
            abstract: true,
            templateUrl: "templates/menu.html",
            controller: 'AppCtrl'
        })

        .state('app.about', {
            url: "/about",
            views: {
                'menuContent': {
                    templateUrl: "templates/about.html"

                }
            }
        })

        .state('app.weekly', {
            url: "/weekly",
            views: {
                'menuContent': {
                    templateUrl: "templates/weekly.html"
                }
            }
        })

        .state('app.monthly', {
            url: "/monthly",
            views: {
                'menuContent': {
                    templateUrl: "templates/monthly.html"
                }
            }
        })

        .state('app.annual', {
            url: "/annual",
            views: {
                'menuContent': {
                    templateUrl: "templates/annual.html"
                }
            }
        })

        .state('app.jira', {
            url: "/jira",
            views: {
                'menuContent': {
                    templateUrl: "templates/jira.html"
                }
            }
        })

        .state('app.staff', {
            url: "/staff",
            views: {
                'menuContent': {
                    templateUrl: "templates/staff.html",
                    controller: 'staffCtrl'
                }
            }
        })

        .state('app.daily', {
            url: "/daily",
            views: {
                'menuContent': {
                    templateUrl: "templates/daily.html",
                    controller: 'DailyStatsCtrl'
                }
            }
        })

        .state('app.mechanical', {
            url: "/mechanical",
            views: {
                'menuContent': {
                    templateUrl: "templates/mechanical.html"
                }
            }
        })

        .state('app.electrical', {
            url: "/electrical",
            views: {
                'menuContent': {
                    templateUrl: "templates/electrical.html"
                }
            }
        })

        .state('app.server', {
            url: "/server",
            views: {
                'menuContent': {
                    templateUrl: "templates/server.html"
                }
            }
        })

        .state('app.calender', {
            url: "/calender",
            views: {
                'menuContent': {
                    templateUrl: "templates/calender.html"
                }
            }
        })

        .state('app.search', {
            url: "/search",
            views: {
                'menuContent': {
                    templateUrl: "templates/search.html"
                }
            }
        })

        .state('app.browse', {
            url: "/browse",
            views: {
                'menuContent': {
                    templateUrl: "templates/browse.html"
                }
            }
        })

        .state('app.playlists', {
            url: "/playlists",
            views: {
                'menuContent': {
                    templateUrl: "templates/playlists.html",
                    controller: 'PlaylistsCtrl'
                }
            }
        })

        .state('app.single', {
            url: "/playlists/:playlistId",
            views: {
                'menuContent': {
                    templateUrl: "templates/playlist.html",
                    controller: 'PlaylistCtrl'
                }
            }
        });

        // if none of the above states are matched, use this as the fallback
        $urlRouterProvider.otherwise('/app/browse');
    });
