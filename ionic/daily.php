<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

$user = 'root';
$pass = 'root';

$dbh = new PDO('mysql:host=localhost;dbname=trackitlive', $user, $pass);

$result = $dbh->prepare("SELECT id, job_no, state,date_required,date_received FROM jobs WHERE state = 'In Calibration'");
$result->execute();

$list = $result->fetchAll();

$outp = "";
foreach($list as $rs){
	if($outp != ""){$outp .= ",";}
	$rec = new DateTime($rs["date_received"]);
	$today = new DateTime(date("Y-m-d"));
	$req = new DateTime($rs["date_required"]);
	$daysLeft = $today->diff($req);

    $outp .= '{"Job_No":"'  		. $rs["job_no"] 		. '",';
    $outp .= '"Id":"'     			. $rs["id"]         	. '",';
    $outp .= '"State":"'   			. $rs["state"] 			. '",';	
    $outp .= '"DaysLeft":"'			. $daysLeft->format('%R%a days') . '",';
    $outp .= '"Date_Received":"'    . $rs["date_received"]  . '",';	
    $outp .= '"Date_Required":"'  	. $rs["date_required"]  . '"}'; 
}

$outp = '{"records":['.$outp.']}';

echo ($outp);

?>