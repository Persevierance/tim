<?php
class Trackit{

	function __construct(){
		if(isset($_GET['action'])) {
			if($_GET['action'] == 'staff'){
			$this->getStaffRecords();
			}
			elseif($_GET['action'] == 'daily'){
				$this->getDailyStats();
			}
		}
	}

	function getStaffRecords(){
		header("Access-Control-Allow-Origin: *");
		header("Content-Type: application/json; charset=UTF-8");

		$conn = new mysqli("localhost", "root", "root", "trackitlive");	

		$result = $conn->query("SELECT id, username, email,first_name,last_name FROM users WHERE status != 'Suspended'");

		$outp = "";
		while($rs = $result->fetch_array(MYSQLI_ASSOC)) {
    		if ($outp != "") {$outp .= ",";}
    		$outp .= '{"Name":"'  		. $rs["username"] 	. '",';
    		$outp .= '"Id":"'     		. $rs["id"]         . '",';
    		$outp .= '"First_Name":"'   . $rs["first_name"] . '",';	
    		$outp .= '"Last_Name":"'    . $rs["last_name"]  . '",';	
    		$outp .= '"Email":"'  		. $rs["email"]      . '"}'; 
		}
		$outp ='{"records":['.$outp.']}';

		$conn->close();

		echo($outp);
	}

	function getDailyStats(){
		header("Access-Control-Allow-Origin: *");
		header("Content-Type: application/json; charset=UTF-8");

		$user = 'root';
		$pass = 'root';

		$dbh = new PDO('mysql:host=localhost;dbname=trackitlive', $user, $pass);

		$result = $dbh->prepare("SELECT id, job_no, state,date_required,date_received FROM jobs WHERE state = 'In Calibration'");
		$result->execute();

		$list = $result->fetchAll();

		$outp = "";
		$count_jobs_incal = 0;
		foreach($list as $rs){
			if($outp != ""){$outp .= ",";}
			$rec = new DateTime($rs["date_received"]);
			$today = new DateTime(date("Y-m-d"));
			$req = new DateTime($rs["date_required"]);
			$daysLeft = $today->diff($req);
			$count_jobs_incal = $count_jobs_incal + 1;

		    $outp .= '{"Job_No":"'  		. $rs["job_no"] 		. '",';
		    $outp .= '"Id":"'     			. $rs["id"]         	. '",';
		    $outp .= '"State":"'   			. $rs["state"] 			. '",';	
		    $outp .= '"DaysLeft":"'			. $daysLeft->format('%R%a days') . '",';
		    $outp .= '"Date_Received":"'    . $rs["date_received"]  . '",';	
		    $outp .= '"Date_Required":"'  	. $rs["date_required"]  . '"}'; 
		}
		$inCal = '{"numberInCal":"' . $count_jobs_incal.'"}';

		$outp = '{"jobsInCal":['.$outp.']}';

echo ($outp);
	}
}$trackit = new Trackit();

?>